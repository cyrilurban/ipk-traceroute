/**
 * @file	trace.hpp
 * @author	CYRIL URBAN
 * @date	2017-04-13
 * @brief	The tracerout program
 */

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <errno.h>
#include <linux/errqueue.h>
#include <sys/time.h>


using namespace std;

typedef struct arguments
{
	char ipInput[INET6_ADDRSTRLEN];
	char type[20];
	int ttlFirst;
	int ttlMax;
} arguments;

arguments argumentProcessing(int argc, char const *argv[]);
int ipv4(struct arguments);
int ipv6(struct arguments);