/**
 * @file	trace.cpp
 * @author	CYRIL URBAN
 * @date	2017-04-13
 * @brief	The tracerout program
 */

#include "trace.hpp"

/**
 * @brief      The main function of tracerout
 *
 * @param[in]  argc  The argc
 * @param      argv  The argv
 *
 * @return     exit value
 */
int main(int argc, const char *argv[])
{

	struct arguments arg = argumentProcessing(argc, argv);
	int ret;

	if (strcmp(arg.type, "ipv6") == 0) {
		ret = ipv6(arg);
	}
	else {
		ret = ipv4(arg);
	}

	return ret;

}


/**
 * @brief      function for IPv4 adress
 *
 * @param[in]  arg   The argument
 *
 * @return     exit value
 */
int ipv4(struct arguments arg) {
	char ipDestination[INET_ADDRSTRLEN];
	char ipRoute[INET_ADDRSTRLEN];
	char one[0];

	// Defining necessary network structures and variables
	struct icmphdr icmpHeader;

	struct addrinfo hints;
	struct addrinfo* adressInfo;
	struct sockaddr_in* rec_sock_addr;
	struct iovec iovecDataArray;
	struct msghdr msgHeader;
	struct cmsghdr *controlMessage;
	struct sock_extended_err *errorSocket;
	struct sockaddr_in endPoint;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;

	iovecDataArray.iov_base = &icmpHeader;
	iovecDataArray.iov_len = sizeof(icmpHeader);

	msgHeader.msg_name = (void*)&endPoint;
	msgHeader.msg_namelen = sizeof(endPoint);
	msgHeader.msg_iov = &iovecDataArray;
	msgHeader.msg_iovlen = 1;
	msgHeader.msg_flags = 0;
	char control[100];
	msgHeader.msg_control = control;
	msgHeader.msg_controllen = sizeof(control);

	// timeout
	fd_set mySet;
	timeval timeout, timeBefore, timeAfter;
	timeout.tv_sec = 2; // timeout = 2 sec

	// call getaddrinfo to fill adressInfo
	if (getaddrinfo(arg.ipInput, "33434", &hints, &adressInfo)) {
		cerr << "Name or service not known." << endl;
		exit(1);
	}

	// Get IPv4 address from adressInfo
	struct sockaddr_in* ip = (struct sockaddr_in *)adressInfo->ai_addr;

	// Get IP destination (transfare address from name to IP)
	inet_ntop(AF_INET, &(ip->sin_addr), ipDestination, INET_ADDRSTRLEN);

	// UDP socket
	int udpSock;
	if ((udpSock = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		cerr << "Error creating host socket." << endl;
		exit(1);
	}

	// time to live
	int ttl = arg.ttlFirst;
	while (ttl <= arg.ttlMax) {

		// set for sending with ttl
		if ((setsockopt(udpSock, IPPROTO_IP, IP_TTL, &ttl, sizeof(ttl)))) {
			cerr << "Error setting TTL." << endl;
			exit(1);
		}

		// time before sending
		gettimeofday (&timeBefore, NULL);

		// send socket
		if ((sendto(udpSock, one, strlen(one), 0, adressInfo->ai_addr, adressInfo->ai_addrlen)) == -1) {
			cerr << "Error sending message." << endl;
			exit(1);
		}

		// set for receiving
		if ((setsockopt(udpSock, IPPROTO_IP, IP_RECVERR, &ttl, sizeof(ttl)))) {
			cerr << "Error setting TTL." << endl;
			exit(1);
		}

		// set timeout
		FD_ZERO(&mySet);
		FD_SET(udpSock, &mySet);
		timeout.tv_sec = 2; // timeout = 2 sec

		// break -> receive icmp or timeout 2 sec
		while (1) {

			// bug, sometimes bits here
			if (select(udpSock + 1, &mySet, NULL, NULL, &timeout) < 0) {
				cerr << "Error in select" << endl;
				return 1;
			}

			if (FD_ISSET(udpSock, &mySet)) {

				// receiving errors
				if ((recvmsg(udpSock, &msgHeader, MSG_ERRQUEUE)) != -1) {

					for (controlMessage = CMSG_FIRSTHDR(&msgHeader); controlMessage; controlMessage = CMSG_NXTHDR(&msgHeader, controlMessage)) {

						// receiving time
						gettimeofday (&timeAfter, NULL);

						// We received an error
						if ((controlMessage->cmsg_level == IPPROTO_IP) && (controlMessage->cmsg_type == IP_RECVERR)) {

							// get IP from error socket
							errorSocket = (struct sock_extended_err*)CMSG_DATA(controlMessage);
							rec_sock_addr = (struct sockaddr_in*)(errorSocket + 1);
							inet_ntop(AF_INET, &rec_sock_addr->sin_addr, ipRoute, sizeof(ipRoute));

							// time result
							float timeResult = timeAfter.tv_sec - timeBefore.tv_sec + timeAfter.tv_usec - timeBefore.tv_usec;

							// converte time to time ms
							timeResult = timeResult / 1000;
							if (timeResult < 0 ) { timeResult += 1000;}

							// Handle ICMP type of error
							switch (errorSocket->ee_type) {

							// host unreachale
							case ICMP_HOST_UNREACH:
								if (ttl < 10)
									printf(" %d   H!\n", ttl);
								else
									printf("%d   H!\n", ttl);
								break;

							// network unreachale
							case ICMP_NET_UNREACH:
								if (ttl < 10)
									printf(" %d   N!\n", ttl);
								else
									printf("%d   N!\n", ttl);
								break;

							// porotocol unreachale
							case ICMP_PROT_UNREACH :
								if (ttl < 10)
									printf(" %d   P!\n", ttl);
								else
									printf("%d   P!\n", ttl);
								break;

							// communication administratively prohibited
							case ICMP_PKT_FILTERED:
								if (ttl < 10)
									printf(" %d   X!\n", ttl);
								else
									printf("%d   X!\n", ttl);
								break;

							default:

								// get domain name from IP
								struct in_addr ip;
								struct hostent *hp;
								char *dnsname;

								if (!inet_aton(ipRoute, &ip)) {
									dnsname = ipRoute;
								}

								if ((hp = gethostbyaddr((const void *)&ip, sizeof ip, AF_INET)) == NULL) {
									dnsname = ipRoute;
								}
								else {
									dnsname = hp->h_name;
								}

								// print IP address (0-9)
								if (ttl < 10) {
									printf(" %d   %s (%s)   %.3f ms\n", ttl, dnsname, ipRoute, timeResult);
								}
								// print IP address (more then 10)
								else {
									printf("%d   %s (%s)   %.3f ms\n", ttl, dnsname, ipRoute, timeResult);
								}

								// if IP adress is last adress -> exit
								if (strcmp(ipDestination, ipRoute) == 0) {
									close(udpSock);
									return 0;
								}
							}
						}
					}
					// next time to live
					ttl++;
					break;
				}
			}
			else {
				// timeout
				if (ttl < 10)
					printf(" %d   *\n", ttl);
				else
					printf("%d   *\n", ttl);
				// next time to live
				ttl++;
				break;
			}
		}
	}
}

/**
 * @brief      function for IPv6 adress
 *
 * @param[in]  arg   The argument
 *
 * @return     exit value
 */
int ipv6(struct arguments arg) {

	char ipDestination[INET6_ADDRSTRLEN];
	char ipRoute[INET6_ADDRSTRLEN];
	char one[0];

	// Defining necessary network structures and variables for ipv6
	struct icmp6_hdr *icmp6Header;
	struct addrinfo hints;
	struct addrinfo* adressInfo;
	struct sockaddr_in6* rec_sock_addr;
	struct iovec iovecDataArray;
	struct msghdr msgHeader;
	struct cmsghdr *controlMessage;
	struct sock_extended_err *errorSocket;
	struct sockaddr_in6 endPoint;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET6; //6
	hints.ai_socktype = SOCK_DGRAM;

	iovecDataArray.iov_base = &icmp6Header;
	iovecDataArray.iov_len = sizeof(icmp6Header);

	msgHeader.msg_name = (void*)&endPoint;
	msgHeader.msg_namelen = sizeof(endPoint);
	msgHeader.msg_iov = &iovecDataArray;
	msgHeader.msg_iovlen = 1;
	msgHeader.msg_flags = 0;
	char control[100];
	msgHeader.msg_control = control;
	msgHeader.msg_controllen = sizeof(control);

	// timeout
	fd_set mySet;
	timeval timeout, timeBefore, timeAfter;
	timeout.tv_sec = 2; // timeout = 2 sec

	// call getaddrinfo to fill adressInfo
	if (getaddrinfo(arg.ipInput, "33434", &hints, &adressInfo)) {
		cerr << "Name or service not known." << endl;
		exit(1);
	}

	// Get IPv6 address from adressInfo
	struct sockaddr_in6* ip = (struct sockaddr_in6 *)adressInfo->ai_addr;

	// Get IP destination (transfare address from name to IP)
	inet_ntop(AF_INET6, &(ip->sin6_addr), ipDestination, INET_ADDRSTRLEN);

	// UDP socket
	int udpSock;
	if ((udpSock = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
		cerr << "Error creating host socket." << endl;
		exit(1);
	}

	// time to live
	int ttl = arg.ttlFirst;
	while (ttl <= arg.ttlMax) {

		// set for sending with ttl
		if ((setsockopt(udpSock, IPPROTO_IPV6, IPV6_UNICAST_HOPS, &ttl, sizeof(ttl)))) {
			cerr << "Error setting TTL." << endl;
			exit(1);
		}

		// time before sending
		gettimeofday (&timeBefore, NULL);

		// send socket
		if ((sendto(udpSock, one, strlen(one), 0, adressInfo->ai_addr, adressInfo->ai_addrlen)) == -1) {
			cerr << "Error sending message." << endl;
			exit(1);
		}

		// set for receiving
		if ((setsockopt(udpSock, IPPROTO_IPV6, IPV6_RECVERR, &ttl, sizeof(ttl)))) {
			cerr << "Error setting TTL." << endl;
			exit(1);
		}

		// set timeout
		FD_ZERO(&mySet);
		FD_SET(udpSock, &mySet);
		timeout.tv_sec = 2; // timeout = 2 sec

		// break -> receive icmp or timeout 2 sec
		while (1) {

			// bug, sometimes bits here
			if (select(udpSock + 1, &mySet, NULL, NULL, &timeout) < 0) {
				cerr << "Error in select" << endl;
				return 1;
			}

			if (FD_ISSET(udpSock, &mySet)) {

				// receiving errors
				if ((recvmsg(udpSock, &msgHeader, MSG_ERRQUEUE)) != -1) {

					for (controlMessage = CMSG_FIRSTHDR(&msgHeader); controlMessage; controlMessage = CMSG_NXTHDR(&msgHeader, controlMessage)) {

						// receiving time
						gettimeofday (&timeAfter, NULL);

						// We received an error
						if ((controlMessage->cmsg_level == IPPROTO_IPV6) && (controlMessage->cmsg_type == IPV6_RECVERR)) {

							// get IP from error socket
							errorSocket = (struct sock_extended_err*)CMSG_DATA(controlMessage);
							rec_sock_addr = (struct sockaddr_in6*)(errorSocket + 1);
							inet_ntop(AF_INET6, &rec_sock_addr->sin6_addr, ipRoute, sizeof(ipRoute));

							// time result
							float timeResult = timeAfter.tv_sec - timeBefore.tv_sec + timeAfter.tv_usec - timeBefore.tv_usec;

							// converte time to time ms
							timeResult = timeResult / 1000;
							if (timeResult < 0 ) { timeResult += 1000;}

							// Handle ICMP type of error
							switch (errorSocket->ee_type) {

							// host unreachale
							// case ICMP_HOST_UNREACH:
							// 	if (ttl < 10)
							// 		printf(" %d   H!\n", ttl);
							// 	else
							// 		printf("%d   H!\n", ttl);
							// 	break;

							// network unreachale
							case ICMP_NET_UNREACH:
								if (ttl < 10)
									printf(" %d   N!\n", ttl);
								else
									printf("%d   N!\n", ttl);
								break;

							// porotocol unreachale
							case ICMP_PROT_UNREACH :
								if (ttl < 10)
									printf(" %d   P!\n", ttl);
								else
									printf("%d   P!\n", ttl);
								break;

							// communication administratively prohibited
							case ICMP_PKT_FILTERED:
								if (ttl < 10)
									printf(" %d   X!\n", ttl);
								else
									printf("%d   X!\n", ttl);
								break;

							default:

								// get domain name from IP
								struct in6_addr ip;
								struct hostent *hp;
								char *dnsname;

								if (!inet_pton(AF_INET6, ipRoute, &ip)) {
									dnsname = ipRoute;
								}

								if ((hp = gethostbyaddr((const void *)&ip, sizeof ip, AF_INET6)) == NULL) {
									dnsname = ipRoute;
								}
								else {
									dnsname = hp->h_name;
								}

								// print IP address (0-9)
								if (ttl < 10) {
									printf(" %d   %s (%s)   %.3f ms\n", ttl, dnsname, ipRoute, timeResult);
								}
								// print IP address (more then 10)
								else {
									printf("%d   %s (%s)   %.3f ms\n", ttl, dnsname, ipRoute, timeResult);
								}

								// if IP adress is last adress -> exit
								if (strcmp(arg.ipInput, ipRoute) == 0) {
									close(udpSock);
									return 0;
								}
							}
						}
					}
					// next time to live
					ttl++;
					break;
				}
			}
			else {
				// timeout
				if (ttl < 10)
					printf(" %d   *\n", ttl);
				else
					printf("%d   *\n", ttl);
				// next time to live
				ttl++;
				break;
			}
		}
	}
}

/**
 * @brief      Simply funtion for argument processing
 *
 * @param[in]  argc  The argc
 * @param      argv  The argv
 *
 * @return     processing arguments (in struct)
 */
struct arguments argumentProcessing(int argc, char const * argv[]) {

	struct arguments arg;

	// default
	arg.ttlFirst = 1;
	arg.ttlMax = 30;

	if (argc == 2) {
		// ip
		strcpy(arg.ipInput, argv[1]);
	}
	else if (argc == 4) {

		// max
		if (strcmp(argv[1], "-m") == 0) {
			arg.ttlMax = atoi(argv[2]);
		}
		// first
		else if (strcmp(argv[1], "-f") == 0) {
			arg.ttlFirst = atoi(argv[2]);
		}
		else {
			cerr << "Wrong arguments" << endl;
			exit(1);
		}
		// ip
		strcpy(arg.ipInput, argv[3]);
	}
	else if (argc == 6) {
		// first and max
		if (strcmp(argv[1], "-f") == 0) {
			// first
			arg.ttlFirst = atoi(argv[2]);
			if (strcmp(argv[3], "-m")) {
				// max
				arg.ttlMax = atoi(argv[4]);
				// ip
				strcpy(arg.ipInput, argv[5]);
			}
			else {
				cerr << "Wrong arguments" << endl;
				exit(1);
			}
		}
		// max and first
		else if (strcmp(argv[1], "-m") == 0) {
			// max
			arg.ttlMax = atoi(argv[2]);
			if (strcmp(argv[1], "-f")) {
				// first
				arg.ttlFirst = atoi(argv[4]);
				// ip
				strcpy(arg.ipInput, argv[5]);
			}
			else {
				cerr << "Wrong arguments" << endl;
				exit(1);
			}
		}
		else {
			cerr << "Wrong arguments" << endl;
			exit(1);
		}
	}
	else {
		cerr << "Wrong arguments" << endl;
		exit(1);
	}

	// first ttl > max
	if (arg.ttlFirst > arg.ttlMax) {
		cerr << "Wrong arguments" << endl;
		exit(1);
	}

	// type
	std::string str = arg.ipInput;
	bool exists = str.find(".") != std::string::npos;

	if (exists) {
		strcpy(arg.type, "ipv4");
	}
	else {
		strcpy(arg.type, "ipv6");
	}

	// min and max
	if (arg.ttlFirst < 1) {
		arg.ttlFirst = 1;
	}
	if (arg.ttlMax > 30) {
		arg.ttlMax = 30;
	}

	return arg;
}